/*
 * University of Mary Washinton
 * Jacques Troussard
 * Professor I.Finlayson
 * CPSC340
 * 03/20/2017
 *
 * Assignment 5: Bank Simulator: TimeTool.h
 */

#ifndef TimeTool_H
#define TimeTool_H

class TimeTool{
public:
	TimeTool();

	~TimeTool();

	int timeToMins(int clockTime);

	void printMinsInTime(int minutes);	
};


#endif
