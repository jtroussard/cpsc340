/*
 * University of Mary Washinton
 * Jacques Troussard
 * Professor I.Finlayson
 * CPSC340
 * 03/20/2017
 *
 * Assignment 5: Bank Simulator: TimeTool.cpp
 */
#include <iostream>
#include "timeTool.h"

using namespace std;

// constructors
TimeTool::TimeTool() {
}

TimeTool::~TimeTool() {
}

// convert 24hr clock time into minutes out of 1440 day
int TimeTool::timeToMins(int clockTime) {
	int hour = clockTime/100 * 60;
	int mins = clockTime%100;
	return hour + mins;
}

// convert minutes (out of 1440) into 24 hour clock format and print
void TimeTool::printMinsInTime(int minutes) {
	int hour = minutes/60;
	int mins = minutes%60;
	if (mins < 10) {
		cout << hour << ":0" << mins;
	} else {
		cout << hour << ":" << mins;
	}
}
