/*
 * University of Mary Washinton
 * Jacques Troussard
 * Professor I.Finlayson
 * CPSC340
 * 03/20/2017
 *
 * Assignment 5: Bank Simulator: queue.h
 */

#ifndef QUEUE_H
#define QUEUE_H

#include "client.h"
#include "timeTool.h"

class Queue{
public:
	Queue();

	~Queue();

	Queue(const Queue& other);
	Queue& operator=(const Queue& other);

	void enqueue(Client c, TimeTool tt, int minutes);

	void enqueue(Client c);

	Client dequeue(int minutes, TimeTool tt);

	Client dequeue();

	bool isEmpty();

	void printQ();

	Client whoIsFirst();

	int getSize();

	bool isFirst(Client c);
	
private:
	struct Node {
		Client client;
		Node* next;
	};

	Node* head;
	int size;

};


#endif
