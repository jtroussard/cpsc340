/*
 * University of Mary Washinton
 * Jacques Troussard
 * Professor I.Finlayson
 * CPSC340
 * 03/20/2017
 *
 * Assignment 5: Bank Simulator: client.cpp
 */
#include "client.h"
#include <iostream>
#include <cstring>

using namespace std;

// constructors
Client::Client() {
	name = NULL;
	arrival = 0;
	duration = 0;
}

Client::Client(char* inName, int inArrival, int inDuration) {
	name = new char[strlen(inName)]; // creating array object of type char
	strcpy(name, inName); // initializing type my copying input argument
	arrival = inArrival;
	duration = inDuration;
}

// copy constructor
Client::Client(const Client& other) {
	name = new char[strlen(other.name)];
	strcpy(name, other.name);
	arrival = other.arrival;
	duration = other.duration;
}

// assignemnt operator overload
Client& Client::operator=(const Client& other) {
	if (this == &other) {
		return *this;
	}
	delete [] name;
	name = new char[strlen(other.name)];
	strcpy(name, other.name);
	arrival = other.arrival;
	duration = other.duration;	

	return *this;
}

// destructor
Client::~Client() {
	delete [] name;
}

// getters
char* Client::getName() {
	return name;
}

int Client::getArrival() {
	return arrival;
}

int Client::getDuration() {
	return duration;
}

// mass setter
void Client::setFields(char* newName, int newArrival, int newDuration) {
	strcpy(name, newName);
	arrival = newArrival;
	duration = newDuration;
}

// never got this to work, speak with professor about how passing references work when calling, not passing.
void Client::decrementDuration(int &newDuration) {
	duration = newDuration;
}
;
