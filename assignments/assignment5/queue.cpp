/*
 * University of Mary Washinton
 * Jacques Troussard
 * Professor I.Finlayson
 * CPSC340
 * 03/20/2017
 *
 * Assignment 5: Bank Simulator: queue.cpp
 */
#include "queue.h"
#include "client.h"
#include "timeTool.h"
#include <iostream>
#include <typeinfo>
#include <cstring>

using namespace std;

// constructors
Queue::Queue() {
	head = NULL;
	size = 0;
}

// destructor
Queue::~Queue() {
	while (size > 0) {
		dequeue();
	}
}

// copy constructor
Queue::Queue(const Queue& other) {
	Node* current = other.head;
	while (current->next != NULL) {
		enqueue(current->client);
		current = current->next;
	}
	enqueue(current->client);
}

// assignment operator overload
Queue& Queue::operator=(const Queue& other) {
	while (head->next != NULL) {
		dequeue();
	}
	Node* current = other.head;
	while (current->next != NULL) {
		enqueue(current->client);
		current = current->next;
	}
	enqueue(current->client);
	return *this;
}

// enqueue client and print statement
void Queue::enqueue(Client c, TimeTool tt, int minutes) {
	// create new node and load member vars
	Node* node = new Node;
	node->client = c;
	node->next = NULL;

	// if the line is empty set this node to the front of the line
	if (isEmpty()) {
		head = node;
	// if there are other nodes in this line, queue node at the end of the line
	} else {
		Node* current = head;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = node;
	}

	// increment the size queue member var
	size++;

	// print to console
	cout << c.getName() << " got in line at ";
	tt.printMinsInTime(minutes);
	cout << "." << endl;
}

// basic equeue without print statement: used during development
void Queue::enqueue(Client c) {
	Node* node = new Node;
	node->client = c;
	node->next = NULL;
	if (isEmpty()) {
		head = node;
	} else {
		Node* current = head;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = node;
	}
	size++;
}

// dequeue client with print statement
Client Queue::dequeue(int minutes, TimeTool tt) {
	// create helper variable to hold the disconnected node
	Node* curr = head;

	// if the line is empty return a null
	if (isEmpty()) {
		return curr->client;
	// if there are nodes in the list set the head nodes next as the new head, disconnecting the old head which then can be returned, then print ot console
	} else {
		cout << head->client.getName() << " is done at ";
		tt.printMinsInTime(minutes);
		cout << "." << endl;
		head = curr->next;
		size--;
		return curr->client;
	}
}

// basic dequeue without print statement: used during development
Client Queue::dequeue() {
	Node* curr = head;
	if (isEmpty()) {
		return curr->client;
	} else {
		head = curr->next;
		size--;
		return curr->client;
	}
}

// return bool according to line being empty or not
bool Queue::isEmpty() {
	if (size == 0) {
		return true;
	} else {
		return false;
	}
}

// print to console the entire line: used during development for debugging purposes
void Queue::printQ() {
	Node* curr = new Node;
	curr = head;
	cout << "LINE:" << endl;
	int lineNum = 1;
	while (curr->next != NULL) {
		cout << "\t" << lineNum << ": " << curr->client.getName() << ", " << curr->client.getArrival() << ", " << curr->client.getDuration() << endl;
		curr = curr->next;
		lineNum++;
	}
	cout << "\t" << lineNum << ": " << curr->client.getName() << ", " << curr->client.getArrival() << ", " << curr->client.getDuration() << endl;
}

// return client without removing from line: used for comparisons and logic in main
Client Queue::whoIsFirst() {
	return head->client;
}

// return size of line: used during development and for debugging purposes
int Queue::getSize() {
	return size;
}

// return bool according to compared name string to who is first in the queue: used to reset teller counter
bool Queue::isFirst(Client c) {
	if (strcmp(head->client.getName(),c.getName())==0) {
		return true;
	} else {
		return false;
	}
}
