/*
 * University of Mary Washinton
 * Jacques Troussard
 * Professor I.Finlayson
 * CPSC340
 * 03/20/2017
 *
 * Assignment 5: Bank Simulator: client.h
 */
#ifndef CLIENT_H
#define CLIENT_H

class Client {
public:
	// empty client object
	Client( );

	// basic client object
	Client(char* name, int arrival, int duration);

	// delete the client
	~Client();

	Client& operator=(const Client& other);

	Client(const Client& other);	

	char* getName();

	int getArrival();

	int getDuration();

	void setFields(char* name, int in, int duration);

	void decrementDuration(int &newDuration);

private:
	char* name;
	int arrival;
	int duration;
};


#endif
