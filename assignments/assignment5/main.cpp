/*
 * University of Mary Washinton
 * Jacques Troussard
 * Professor I.Finlayson
 * CPSC340
 * 03/20/2017
 *
 * Assignment 5: Bank Simulator: main.cpp
 */

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include "client.h"
#include "queue.h"
#include "timeTool.h"

using namespace std;

int main(int argc, char* argv[]) {
	// intake arg as read file
	ifstream inFile(argv[1]);
	
	// vars for loading info into client objects
	char iName[30];
	int iArrive;
	int iDuration;

	// simulated bank line
	Queue bankLine;

	// time converter tool object
	TimeTool tt;

	// check to see is file was found
	if (!inFile.is_open())
	{
		cout << "Error opening file: " << argv[1] << endl;
	} else {
		// if found load client on deck - which mean they are not in line but next to enter simulation
		inFile >> iName >> iArrive >> iDuration;
		Client onDeck(iName, iArrive, iDuration);

		// create teller_coutner object for simulated teller
		int teller_coutner = 0;

		// for loop for each minute in the day starting at 540 which represents opening hour in minutes and ending at 1020 which represents closing hour in minutes
		for (int i = 539; i < 1021; i++) {
			/* the following code was print statements used for debugging

			// cout << "=========== TURN NUMBER " << i << " ===========" << endl;
			// cout << "\tTHE TIME IS ";
			// tt.printMinsInTime(i);
			// cout << endl;
			// cout << "\tCOUNTER IS @ " << teller_coutner << endl;
			// if (bankLine.isEmpty()) {
			// 	cout << "\tLINE IS EMPTY" << endl;
			// } else {
			// 	bankLine.printQ();
			// }

			*/

			// check client on decks arrival time and compare with simulated time of day
			int timeInMins = tt.timeToMins(onDeck.getArrival());
			if (i == timeInMins) {
				// if on deck client is due to arrive, create client object and load data
				Client &newClient = onDeck;

				// enqueue client into bankline
				bankLine.enqueue(newClient, tt, timeInMins);
					
					// if client enqueued is the only client in line, load duration time into teller var
					if (bankLine.isFirst(newClient)) {
						teller_coutner = newClient.getDuration();
					}

				// now that ondeck client is in simulation, load the next client	
				inFile >> iName >> iArrive >> iDuration;
				onDeck.setFields(iName, iArrive, iDuration);
			}

			//  perform bank lien simulation operations, if the line isn't empty
			if (!bankLine.isEmpty()) {

				// is the teller is finished with client, dequeue client from line
				if (teller_coutner == 0) {
					bankLine.dequeue(i, tt);

					// if there are no more clients in the line, free the teller up
					if (bankLine.isEmpty()){
						teller_coutner = 0;
					// if there are clients in the line, load the head client into the teller var
					} else {
						teller_coutner = bankLine.whoIsFirst().getDuration();
					}
				}

				// decrement the teller to simulate a minute passing
				teller_coutner --;
			}
		}
	}
	return 0;
}
