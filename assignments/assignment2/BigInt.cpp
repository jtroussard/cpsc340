/*
 * University of Mary Washinton
 * Jacques Troussard
 * Professor I.Finlayson
 * CPSC340
 * 02/21/2017
 *
 * Assignment 2: BigInt
 */
#include <iostream>
#include <cstring>
using namespace std;

class BigInt {
public:
	//Default constructor. Sets BigInt to 0.
	BigInt() {

		d_array = new int[size];
		d_array[0] = 0;
	}

	//Integer type param constructor
	BigInt(int number) {
		int digits = 0;
		for (int i = number; i > 0 ; i /= 10) {
			digits++;
		}
		size = digits;
		d_array = new int[size];
		int result = number;
		for (int i = digits - 1; i >= 0; i--) {
			d_array[i] = result % 10;
			result /= 10;
		}
	}

	//C-String type param constructor
	BigInt(const char cStrNum []) {
		size = strlen(cStrNum);
		d_array = new int[size];
		for (int i = size - 1; i >= 0; i--) {
			// convert ascii code into corresponding integer
			int incoming = cStrNum[i] - '0';
			// insert the integer into the int array
			d_array[i] = incoming;
		}
	}

	//Copy Constructor
	BigInt(const BigInt& other) {
		size = other.getSize();
		d_array = new int[size];
		for (int i = size - 1; i >= 0; i--) {
			d_array[i] = other.getIndex(i);
		}
	}
	
	//Assignment Operator
	BigInt& operator=(const BigInt &other) {
		delete [] d_array;
		size = other.getSize();
		d_array = new int [size];
		for (int i = size - 1; i >= 0; i--) {
			d_array[i] = other.getIndex(i);
		}
		return *this;
	}

	//Destructor
	~BigInt() {
		delete [] d_array;
	}

	//Helping methods

	int getSize() const {
		return size;
	}

	int getIndex(int index) const {
		return d_array[index];
	}

	void setIndex(int num, int index) {
		if (!(index < 0)) {
			d_array[index] = num;
		}
	}

	/*
	 * This method is used in the addition operator override. When, on the 
	 * largest number being operated on, a carry over is detected at the 
	 * first(highest) significant figure the original dynamic array has not 
	 * enough memeory allocated to accomadate the resulting sum. Its' memory is 
	 * deallcated, and its' pointer is redirected to an array of approperiate 
	 * size. Finally the BigInts instance variable size, is updated to reflect 
	 * the new size of the dynamic array.
	 */
	void setArrayPointer(int* replacement, int new_size) {
		//Because 
		delete [] d_array;
		d_array = replacement;
		size = new_size;
	}

private:
	//Size of array
	int size;
	//Pointer to array
	int* d_array;
};

// the output << operator
ostream& operator<<(ostream& os, const BigInt& number) {
	int index = number.getSize();
	for (int i = 0; i < index; i++) {
		os << number.getIndex(i);
	}
	return os;
}

// the input >> operator
istream& operator>>(istream& is, BigInt& bi) {
	//Allocated 100 spaces as per assignment sheet
	char input[101];
	//Read input from user
	is >> input;
	//Create new BigInt with user input
	BigInt temp(input);
	//Set original BigInt equal to temp
	bi = temp;
	return is;
}

// the comparator operators
bool operator<(const BigInt& left, const BigInt right) {
	bool flag = false;
	//First check cardinal size of BigInts
	if (left.getSize() < right.getSize()) {
		flag = true;
	//If cardinality is the same, compare each index.
	} else if (left.getSize() == right.getSize()) {
		int index = 0;
		int left_digit = left.getIndex(index);
		int right_digit = right.getIndex(index);
		//While the index values are equal, move to the next highest sig fig
		while (left_digit == right_digit) {
			index += 1;
			/*
			 * If index is equal to size the numbers are equal and there fore
			 * neither can be less than eachother. Flag is returned as false.
			 */
			if (index == left.getSize()) {
				return flag;
			}
			left_digit = left.getIndex(index);
			right_digit = right.getIndex(index);
		}
		//Compare indexes when a difference is found and set flag.
		if (left_digit < right_digit) {
			flag = true;
		}
	}
	return flag;
}

bool operator>=(const BigInt& left, const BigInt& right) {
	return !(left < right);
}

bool operator>(const BigInt& left, const BigInt& right) {
	return (right < left);
}

bool operator<=(const BigInt& left, const BigInt& right) {
	return !(right < left);
}

bool operator!=(const BigInt& left, const BigInt& right) {
	return (left < right) || (right < left);
}

bool operator==(const BigInt& left, const BigInt& right) {
	return !(left < right) && !(right < left);
}

// the addition operator
BigInt operator+(const BigInt& left, const BigInt& right) {
	int carry = 0;
	int top_idx;
	int bottom_idx;
	int temp_size;
	int sum;
	BigInt top;
	BigInt bottom;

	/*
	 * Determine which BigInt has more digits to help estimate the number of 
	 * digits the sum will be.
	 */
	if (left >= right) {
		top = left;
		bottom = right;
		temp_size = top.getSize();
		top_idx = temp_size - 1;
		bottom_idx = bottom.getSize() - 1;
	} else {
		top = right;
		bottom = left;
		temp_size = top.getSize();
		bottom_idx = bottom.getSize() - 1;
		top_idx = temp_size - 1;
	}

	//Create loop control for maximum number of addition operations
	int num_of_ops = temp_size + 1;
	for (int i = 0; i < num_of_ops; ++i) {
		/*
		 * If during this cycle of addition operation the bottom number still 
		 * has digits, add them with their cooresponding top digit.
		 */
		if (bottom_idx >= 0) {
			sum = top.getIndex(top_idx) + bottom.getIndex(bottom_idx) + carry;
			// Record carry over if applicable and adjust sum var.
			if (sum >= 10) {
				carry = 1;
				sum %= 10;
			} else {
				carry = 0;
			}
		/*
		 * If during this cycle of addition operation the bottom number has
		 * no more digits, and yet the top number still has digits, continue
		 * with addition operation between the top digit and the value of 
		 * the carry over variable, regardless of value.
		 */
		} else if (top_idx >= 0) {
			sum = top.getIndex(top_idx) + carry;
			// Record carry over if applicable and adjust sum var.
			if (sum >= 10) {
				carry = 1;
				sum %= 10;
			} else {
				carry = 0;
			}
		/*
		 * Finally if both top and bottom numbers digits have been exhausted
		 * yet the carry over variables value is not 0, increase the size of
		 * the return BigInt, and insert the carry variable into the highest
		 * significate value spot.
		 */
		} else if (carry == 1) {
			//Move the value of carry into the sum variable.
			sum = carry;
			//Create a replacement array to pass into the setArrayPoint method.
			int* temp = new int[top.getSize() + 1];
			for (int i = 0; i < top.getSize(); ++i)
			{
				temp[i + 1] = top.getIndex(i);
			}

			top.setArrayPointer(temp, top.getSize() + 1);
			top_idx = 0;
		}

		//Replace return BigInt index value of this cycle with sum value.
		top.setIndex(sum, top_idx);
		//Decrement the index counters, to move to the next highest digit.
		top_idx--;
		bottom_idx--;
	}
	return top;
}





int main( ) {
	// create two objects with the default constructor
	BigInt a, b;

	// input the bigints
	cout << "Enter two BigInts: ";
	cin >> a >> b;

	// test compariosns and printing
	cout << a << " is " << ((a < b) ? "" : "not ") << "less than " << b << endl;
	cout << a << " is " << ((a > b) ? "" : "not ") << "greater than " << b << endl;
	cout << a << " is " << ((a <= b) ? "" : "not ") << "less than or equal to " << b << endl;
	cout << a << " is " << ((a >= b) ? "" : "not ") << "greater than or equal to " << b << endl;
	cout << a << " is " << ((a == b) ? "" : "not ") << "equal to " << b << endl;
	cout << a << " is " << ((a != b) ? "" : "not ") << "not equal to " << b << endl;

	// test addition
	cout << a << " + " << b << " = " << a + b << endl;

	// test value constructors
	const BigInt c(123456), d("123456789012345678901234567890123456789012345678901234567890");
	cout << c + d << endl;

	return 0;
}
