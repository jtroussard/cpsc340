#ifndef SONG_H
#define SONG_H

class Song {
public:
    // constructors
    Song( );
    Song(char artist[], char song[]);

    // destructor
    ~Song();

    //test print object
    void printEntry();

    // compare song titles 0 = same
    int compareSongTitle(char songName[]);

    // return song name pointer
    char* getSongNamePtr();

    // flip bool
    void flip();

    // check bool     
    bool getBool();

private:
    char artist[100];
    char song[100];
    bool shuffled;
};

#endif
