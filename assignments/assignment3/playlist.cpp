// playlist.cpp

#include <cstring>
#include <iostream>
#include <stdlib.h>
#include "playlist.h"
#include "song.h"
using namespace std;

// make an empty list
Playlist::Playlist() {
    head = NULL;
    tail = NULL;
}

// delete the list
Playlist::~Playlist() {
    // remove first one until there is no first one
    while (head != NULL) {
        char songToRemove[100];
        strcpy(songToRemove, head->data.getSongNamePtr());
        remove(songToRemove);
    }
}


// copy constructor
Playlist::Playlist(const Playlist& other) {
    Node* current = other.head;
    head = tail = NULL;
    while (current != NULL) {
        addAtEnd(current->data);
        current = current->next;
    }
}


// assignment operator
Playlist& Playlist::operator=(const Playlist& other) {
    // clear the list
    while (head != NULL) {
        char songToRemove[100];
        strcpy(songToRemove, head->data.getSongNamePtr());
        remove(songToRemove);
    }

    // copy over the other
    Node* current = other.head;
    head = tail = NULL;
    while (current != NULL) {
        addAtEnd(current->data);
        current = current->next;
    }

    return *this;
}


// add an element to the start
void Playlist::addAtStart(Song entry) {
    // make the node
    Node* node = new Node;
    node->data = entry;
    node->prev = NULL;

    // if head is NULL, it's the ONLY one
    if (head == NULL) {
        head = node;
        tail = node;
        node->next = NULL;
    } else {
        node->next = head;
        head->prev = node;
        head = node;
    }
}

// add an element to the end
void Playlist::addAtEnd(Song entry) {
    // make the node
    Node* node = new Node;
    node->data = entry;
    node->next = NULL;

    // if tail is NULL, it's the ONLY one
    if (tail == NULL) {
        head = node;
        tail = node;
        node->prev = NULL;
    } else {
        node->prev = tail;
        tail->next = node;
        tail = node;
    }
}

// remove an element
void Playlist::remove(char songName[]) {
    // find the node with our entry
    Node* current = head;

    while (current != NULL) {
        if (current->data.compareSongTitle(songName) == 0) {
            // FOUND IT

            // if the previous is NULL, we are removing head!
            if (current->prev == NULL) {
                head = current->next;
                if (head != NULL) {
                    head->prev = NULL;
                } else {
                    tail = NULL;
                }
            } else {
                // point previous's next pointer at the one after current
                current->prev->next = current->next;

                // also point next's previous pointer to current's prev
                if (current->next != NULL) {
                    current->next->prev = current->prev;
                } else {
                    tail = current->prev;
                }
            }

            // delete node
            delete current;
            cout << songName << " was removed from the list." << endl << endl << endl;
            return;
        }

        current = current->next;
    }

    // if we got here we didn't find it!
    cout << songName << " was not found in the list!" << endl << endl << endl;
}


// print the list Forward
void Playlist::printForward() const {
    // the node we are currently on
    Node* current = head;

    // while we're not at the end
    while (current != NULL) {
        // print the data portion
        current->data.printEntry();

        // move onto the next one
        current = current->next;
    }

    cout << endl << endl;
}

// print the list Backward
void Playlist::printBackward() const {
    // the node we are currently on
    Node* current = tail;

    // while we're not at the start
    while (current != NULL) {
        // print the data portion
        current->data.printEntry();

        // move back to the previous one
        current = current->prev;
    }

    cout << endl << endl;
}

int Playlist::count() const {
    // the node we are currently on
    Node* current = head;
    int count = 0;

    // while we're not at the end
    while (current != NULL) {
        // count the node
        count++;

        // move onto the next one
        current = current->next;
    }

    return count;
}

// 'shuffle' playlist
void Playlist::shuffle() const {
    int numberOfSongs = Playlist::count();
    while (numberOfSongs > 0) {
        Node* current = head;
        
        int randnum;
        int counter = 1;
        randnum = rand() % Playlist::count() + 1;
        // cout << "Searching for " << randnum << endl;

        // while we're not at the random node
        while (counter != randnum) {
            // move onto the next one
            current = current->next;
            counter++;
        }

        if (!current->data.getBool())
        {
            current->data.printEntry();
            current->data.flip();
            numberOfSongs--;
        }
        
    }
    resetBool();
}

void Playlist::resetBool() const {
    // the node we are currently on
    Node* current = head;

    // while we're not at the end
    while (current != NULL) {
        // count the node
        current->data.flip();

        // move onto the next one
        current = current->next;
    }
}
