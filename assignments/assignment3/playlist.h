// Playlist.h
#ifndef PLAYLIST_H
#define PLAYLIST_H

#include "song.h"
#include <iostream>
using namespace std;

class Playlist {
    public:
        // make an empty list
        Playlist();

        // delete the list
        ~Playlist();

        // copy constructor and operator=
        Playlist(const Playlist& other);
        Playlist& operator=(const Playlist& other);

        // add an element to the start
        void addAtStart(Song entry);

        // add an element to the end
        void addAtEnd(Song entry);

        // remove an element
        void remove(char songName[100]);

        // print the list Forward
        void printForward() const;

        // print the list Backward
        void printBackward() const;

		// count songs
		int count() const;

		// shuffle songs
		void shuffle() const;

        // reset bool var of song after shuffle
        void resetBool() const;

    private:
        struct Node {
            Song data;
            Node* next;
            Node* prev;
        };

        Node* head;
        Node* tail;
};

#endif
