#include <iostream>
#include "song.h"
#include <cstring>
using namespace std;

// constructors
Song::Song() {
    artist[0] = '\0';
    song[0] = '\0';
    shuffled = false;
    //cout << "Default constructor called" << endl;
}

Song::Song(char nameOfArtist [], char nameOfSong []) { 
    strcpy(artist, nameOfArtist);
    strcpy(song, nameOfSong);
    shuffled = false;
    //cout << "Basic constructor called" << endl;
}

// delete the song
Song::~Song() {
    //cout << "Destructor called" << endl;
}

// test print object
void Song::printEntry() {
    cout << "\"" << song << "\" - " << artist << endl;
}

// compare song titles 0 = same
int Song::compareSongTitle(char songName[]) {
    int result = strcmp(song, songName);
    return result;
}

char* Song::getSongNamePtr() {
    char* result = new char[100];
    strcpy(result, song);
    return result;
}

void Song::flip() {
    shuffled=!getBool();
}

bool Song::getBool() {
	return shuffled;
}
;
