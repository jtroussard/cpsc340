#include "song.h"
#include "playlist.h"
#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {

	Playlist list;
	bool flag = true;
	bool inFlag = true;
	int option;

	while (option != 9) {
		if (flag)
		{
			cout << "Welcome to the Playlist. ";
			flag = false;
		}

		cout << "Process command by entering the corresponding number.\n[1] Add\n[2] Remove\n[3] Count\n[4] Play\n[5] Shuffle\n[9] Exit program\nEnter selection > ";

		cin >> option;
		if (cin.fail())
		{
			cin.clear();
			cin.ignore();
			option = 0;
		}

		char songName[100];
		char artistName[100];

		switch (option) {
		case 1: {
			cin.ignore();
			cout << "Enter name of song > ";
			cin.getline(songName, 100);
			cout << "Enter name of band/artist > ";
			cin.getline(artistName, 100);
			Song incomingSong(artistName, songName);
			//cout << songName << "  " << artistName << endl;
			//incomingSong.printEntry();
			list.addAtEnd(incomingSong);
			cout << endl << endl;
			break;
		}
		case 2: {
			cin.ignore();
			cout << "Enter name of song > ";
			cin.getline(songName, 100);
			list.remove(songName);
			break;
		}
		case 3: {
			cout << "There are " << list.count() << " songs in this playlist." << endl << endl << endl;
			break;
		}
		case 4: {
			list.printForward();
			cout << endl << endl;
			break;
		}
		case 5: {
			list.shuffle();
			cout << endl << endl;
			break;
		}
		case 9: {
			cout << "Good-bye!" << endl;
			break;
		}
		case 0: {
			cout << "Sorry, that is an invalid input. Try again." << endl;
			cin.clear();
			cin.ignore();
			cout << endl << endl;
			break;
		}
		default: {
			cout << "Sorry, that is an invalid input. Try again." << endl;
			break;
		}
		}

	}


	return 0;
}
