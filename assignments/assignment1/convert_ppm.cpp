#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>
#include <typeinfo>
#include <stdlib.h>
using namespace std;

class Pix {
	public:
		Pix() {
			red = 0;
			blu = 0;
			grn = 0;
		}

		void setRbg(int r, int b, int g) {
			red = r;
			blu = b;
			grn = g;
		}

		void viewData() {
			cout << "Red: " << red << "\nBlu: " << blu << "\nGrn: " << grn;
		}

		int getRed() {
			return  red;
		}

		int getBlu() {
			return  blu;
		}

		int getGrn() {
			return  grn;
		}

		void greyify() {
			int avg = (red + blu + grn)/3;
			setRbg(avg,avg,avg);
		}
		
		void negative() {
			setRbg(abs(red-255),abs(blu-255),abs(grn-255));
		}

	private:
		int red;
		int blu;
		int grn;
};


int main (int argc, char *argv[]) {

		ifstream inFile(argv[1]);
		if (!inFile.is_open()) {
			cout << "There was a problem opening the file: " << argv[1] << endl;
		}else{
			//create output file, pixel data array, pixel obj array and a line buffer
			cout << argv[1] << " Opened successfully " << endl;
			ofstream grey_out("grayscale.ppm");
			ofstream ngtv_out("extra.ppm");
			cout << "Created Outputfiles... " << endl;
			int data [3][250000];
			Pix img [500][500];
			char line[90];
		
			//burn header lines from ppm file after appending to output file
			inFile.getline(line,90);
			grey_out << line << endl;
			ngtv_out << line << endl;
			inFile.getline(line,90);
			grey_out << line << endl;
			ngtv_out << line << endl;
			inFile.getline(line,90);
			grey_out << line << endl;
			ngtv_out << line << endl;
			cout << "PPM header written to output files... " << endl;
			
			int newnumber; //incoming data from original ppm file
			int row=0;
			int col=0;
		
			//loop through pixel data loading it into a 3 x 250,000 array
			while (inFile>>newnumber){
				//when rbg values fill current column, reset row and increment col
				if (row==3){
					row=0;
					col++;
				}
				//insert data into array (red, blue or green) then increment row
				data[row][col]=newnumber;
				row++;
			}
			cout << "PPM input data read and organized... " << endl;
			
			/*
			 * Using different row and column variables for read ability. This should
			 * help reinforce that the data is read from one array and written into 
			 * a Pix (pixel) object before being loaded into another array, which more
			 * directly represents the converted file. 	
			 */
			int irow=0;
			int icol=0;
			
			//for each pixel in the file
			for (int i=0; i<249999; i++){
				Pix incomingPixel;
				
				//hard coded rows represent the red, blue and green values, 'i' cycles the pixels(columns)
				incomingPixel.setRbg(data[0][i], data[1][i], data[2][i]);
				//convert the pixel data into greyscale
				//incomingPixel.greyify();
			
				//cycle through each cell of the Pix obj array and load incomingPixel object.
				if (icol==500){
					icol=0;
					irow++;
				}
				img[irow][icol]=incomingPixel;
				//move to next cell
				icol++;
			}
			
			//write the Pix obj values to the oputput file in ppm format
			for (int i=0; i<500; i++){
			 	for (int j=0; j<500; j++){
			 		Pix grey = img[i][j];
					grey.greyify();
			 		Pix ngtv = img[i][j];
			 		ngtv.negative();
			 		cout << "Data converted..." << endl;
			 		
			 		grey_out << grey.getRed() << " ";
			 		grey_out << grey.getBlu() << " ";
			 		grey_out << grey.getGrn() << " ";
			 		
			 		ngtv_out << ngtv.getRed() << " ";
			 		ngtv_out << ngtv.getBlu() << " ";
			 		ngtv_out << ngtv.getGrn() << " ";
				}
			}
			cout << "Image conversion complete... " << endl;	
		}
	return 0;
}

