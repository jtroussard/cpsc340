#define _USE_MATH_DEFINES

#include <cmath>
#include "calcstack.h"
#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <math.h>
using namespace std;

int main(int argc, char const *argv[]) {

	// create data structure
	Calcstack stack;

	// get user input
	char input[100];
	bool run = true;
	while (run) {
		stack.resetStack(); // used for program iterations after the first
		cout << "Enter a postfix expression: ";
		cin.getline(input, 100);

		// determine size of user input string
		int input_size = strlen(input);
		if (input_size == 0) {
			run = false;
		}

		// separate, identify, and act on user input by each token,
		// delimited by spaces.
		bool error = false;
		char * pch; //substring pointer to find elements within input
		pch = strtok(input, " ");

		// while the string still has tokens to produce
		while (pch != NULL) {
			double number;
			// if a token represents a number/value, push onto stack
			if (number = atof(pch)) {
				stack.push(number);
			} else if (strcmp(pch, "PI") == 0) {
				stack.push(M_PI);
			} else if (strcmp(pch, "E") == 0) {
				stack.push(M_E);
				// special case: if token represent square root function, pass the
				// case symbol "S" to the switch. Otherwise check token in switch
				// normally.
			} else {
				char symbol;
				if (strcmp(pch, "SQRT") == 0) {
					symbol = 'S';
				} else {
					symbol = *pch;
				}
				// This switch performs the actual operations, matching the
				// operator symbol with the apporperiate mathematical operation
				// pushing the end result back to the stack. If the operation
				// fails due to stack size, unknown symbol, or illegal/undefined
				// operation, a failure boolean flag is triggered to break out
				// of thw switch and ignore the remaining user input expression
				// if any remains. The stack is reset at the top of the while
				// loop when the user is promopted for input again.
				switch (symbol) {
				case '+': {
					//add();
					if (stack.getSize() >= 2)	{
						double a = stack.pop();
						double b = stack.pop();
						double c = a + b;
						stack.push(c);
					} else {
						//invalid expression was entered into calculator
						cout << "Invalid expression: not enough arguments (PLUS)" << endl;
						pch = NULL;
						error = true;
					}
					break;
				}
				case '-': {
					//sub();
					if (stack.getSize() >= 2)	{
						double a = stack.pop();
						double b = stack.pop();
						double c = b - a;
						stack.push(c);
					} else {
						//invalid expression was entered into calculator
						cout << "Invalid expression: not enough arguments (MINUS)" << endl;
						pch = NULL;
						error = true;
					}
					break;
				}
				case '*': {
					//mlt();
					if (stack.getSize() >= 2)	{
						double a = stack.pop();
						double b = stack.pop();
						double c = a * b;
						stack.push(c);
					} else {
						//invalid expression was entered into calculator
						cout << "Invalid expression: not enough arguments (MULT)" << endl;
						pch = NULL;
						error = true;
					}
					break;
				}
				case '/': {
					//div();
					if (stack.getSize() >= 2)	{
						double a = stack.pop();
						double b = stack.pop();
						if (b == 0) {
							cout << "Invalid expression: divid by zero" << endl;
							pch = NULL;
							error = true;
						} else {
							double c = b / a;
							stack.push(c);
						}
					} else {
						//invalid expression was entered into calculator
						cout << "Invalid expression: not enough arguments (DIV)" << endl;
						pch = NULL;
						error = true;
					}
					break;
				}
				case 'S': {
					//sqrt();
					if (stack.getSize() >= 1) {
						double a = stack.pop();
						if (a > 0) {
							double b = sqrt(a);
							stack.push(b);
						} else {
							cout << "Invalid expression: cannot compute imaginary numbers" << endl;
							pch = NULL;
							error = true;
						}
					}
					break;
				}
				case '^': {
					//exp();
					if (stack.getSize() >= 2)	{
						double exponent = stack.pop();
						double base = stack.pop();
						double c = pow(base, exponent);
						stack.push(c);
					} else {
						//invalid expression was entered into calculator
						cout << "Invalid expression: not enough arguments (EXP)" << endl;
						pch = NULL;
						error = true;
					}
					break;
				}
				default: {
					// expression is invalid
					cout << "Invalid expression: Unknown token" << endl;
					pch = NULL;
					error = true;
					break;
				}
				}
			}
			pch = strtok(NULL, " ");
		}
		if (!error)	{
			if (stack.getSize() == 1)	{
				cout << stack.pop() << endl;
			} else if (stack.getSize() != 0) {
				cout << "Invalid expression: Too many elements on stack" << endl;
			}
		} else {
			stack.resetStack();
		}
	}
	cout << "Exiting program" << endl;
	return 0;
}
