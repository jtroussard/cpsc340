// Calcstack.cpp
#include <cstring>
#include <iostream>
#include "calcstack.h"
using namespace std;

// make an empty stack
Calcstack::Calcstack() {
	head = NULL;
	size = 0;
}

// delete the stack
Calcstack::~Calcstack() {
	while (head != NULL) {
		pop();
	}
}

// copy constructor
Calcstack::Calcstack(const Calcstack& other) {
	Node* current = other.head;
	Calcstack temp;
	// copy in reverse order onto temp stack.
	while (current != NULL) {
		temp.push(current->element);
		current = current->under;
	}
	// copy in correct order onto intended stack
	current = temp.head;
	head = NULL;
	while (current != NULL) {
		push(current->element);
		current = current->under;
	}
}

// assignment operator
Calcstack& Calcstack::operator=(const Calcstack& other) {
	// clear stack
	while (head != NULL) {
		pop();
	}
	Node* current = other.head;
	Calcstack temp;
	// copy in reverse order onto temp stack
	while (current != NULL) {
		temp.push(current->element);
		current = current->under;
	}
	// copy in correct order onto intended stack
	current = temp.head;
	head = NULL;
	while (current != NULL) {
		push(current->element);
		current = current->under;
	}
}

// push element onto stack
void Calcstack::push(double element) {
	Node* node = new Node;
	node->element = element;
	// push on empty stack
	if (head == NULL) {
		head = node;
		node->under = NULL;
		// push on non-empty stack
	} else {
		node->under = head;
		head = node;
	}
	size++;
}

// pop element off of stack
double Calcstack::pop() {
	// check for non-empty stack
	if (head != NULL)
	{
		double result = head->element;
		head = head->under;
		if (head == NULL) {
		}
		size--;
		return result;
	} else {
		cout << "Cannot pop from empty stack" << endl;
		return NULL;
	}
}

// print stack newest->oldest
void Calcstack::printStack() {
	// check for non-empty stack
	if (head != NULL)
	{
		int line = 1;
		Node * current = head;
		// start loop and print line by line
		while (current != NULL) {
			cout << line << ": " << current->element << endl;
			line++;
			current = current->under;
		}
	} else {
		cout << "Stack is empty no elements to print." << endl;
	}
}

// count elements in stack
int Calcstack::getSize() {
	return size;
}

// reset the stack
void Calcstack::resetStack() {
	while (head != NULL) {
		pop();
	}
}