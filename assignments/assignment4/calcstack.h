// calcstack.h
#ifndef CALCSTACK_H
#define CALCSTACK_H

#include <iostream>

using namespace std;

class Calcstack {
	public:
		// constructor - default
		Calcstack();

		// destructor - default
		~Calcstack();

		// copy constructor & operator=
		Calcstack(const Calcstack& other);
		Calcstack& operator=(const Calcstack& other);

		// push element onto stack
		void push(double element);

		// pop element off of stack
		double pop();

		// print stack newest->oldest
		void printStack();

		// count elements in stack
		int getSize();

		// reset stack
		void resetStack();

	private:
		struct Node {
			double element;
			Node* under;
		};

		Node* head;
		int size;
	};

#endif
