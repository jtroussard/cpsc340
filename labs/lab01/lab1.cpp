#include <iostream>
using namespace std;

int main() {
	int var1;
	int var2;
	int var3;
	int  var4;
	double result;
	
	cout << "Welcome to the average calculator." << endl;
	cout << "Enter the first number:";
	cin >>  var1;
	cout << "Enter the second number:";
	cin >> var2;
	cout << "Enter the third  number:";
	cin >> var3;
	cout << "Enter the fourth  number:";
	cin >> var4;

	result = ((var1 + var2 + var3 + var4)/4.0);
	cout << (double)result << endl;

	return (0);
}
