//time.cpp

#include <iostream>
using namespace std;


class Time {
	public:
		Time(int h, int m) {
			hours = h;
			minutes = m;
		}

		// check if this time is before another one
		bool before(const Time& other) {
			// check hours
			if(hours < other.hours) {
				return true;
			} else if(hours > other.hours) {
				return false;
			}

			// hours is same - check minutes
			if(minutes < other.minutes) {
				return true;
			} else {
				return false;
			}
		}

		// accessors
		int getHours() const {

			return hours;
		}

		int getMinutes() const {

			return minutes;
		}


	private:
		int hours, minutes;
};

// << override
ostream& operator<<(ostream& os, const Time& time) {

	// add zero to minutes if necessary
	if(time.getMinutes() < 10) {
		os << time.getHours() << ":0" << time.getMinutes();
	}else{

		os << time.getHours() << ":" << time.getMinutes();
	}

	return os;

}

// less than operator override
bool operator<(const Time& a, const Time& b) {

	if (a.getHours()*60+a.getMinutes() < b.getHours()*60+b.getMinutes()) {

		return true;
	}else{
		return false;
	}
}

// using 'less than' template, override remaining comparator operators
bool operator>=(const Time& a, const Time& b) {
	return !(a < b);
}

bool operator>(const Time& a, const Time& b) {
	return (b < a);
}

bool operator<=(const Time& a, const Time& b) {
	return !(b < a);
}

bool operator!=(const Time& a, const Time& b) {
	return (a < b) || (b < a);
}

bool operator==(const Time& a, const Time& b) {
	return !(a < b) && !(b < a);
}


int main() {
	// read input
	int h, m;

    cout << "Enter hours: ";
    cin >> h;
    cout << "Enter minutes: ";
    cin >> m;

    // make the time and print it
    Time t(h, m);
    cout << "The Time is now: " << t << endl;

// use the "before" function
    if(t<(Time(12, 0))) {
        cout << endl << "Good Morning!" << endl;
    } else if(t==(Time(12, 0))) {
	cout << endl << "It's High Noon!" << endl;
    } else if(t<(Time(17, 0))) {
        cout << endl << "Good Afternoon!" << endl;
    } else {
        cout << endl << "Good Night!" << endl;
    }   

    return 0;
}
