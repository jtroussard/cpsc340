#include <iostream>
using namespace std;

int main() {
	int n;

	cout << "How many integers will you be entering? " << endl;
	cin >> n;

	int* array = new int[n];

	for (int i=0; i<n; i++){	
		cout << "Enter element #" << i+1 << "\n>";
		cin >> array[i];
	}	

	int caboose = n-1;
	for (int i=0; i<n; i++){	
		cout << "element at index " << caboose << " is " << array[caboose] << endl;
		caboose--;
	}	
	
	delete [] array;
	//Test to make sure array memory was released
	//for (int i=0; i<n; i++){	
	//	cout << "element " << i << " is " << array[i] << endl;
	//}

	return 0;
}

