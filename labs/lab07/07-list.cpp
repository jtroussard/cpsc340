#include <iostream>
using namespace std;

class DoubleList {
								public:
																// make an empty list
																DoubleList() {
																								head = NULL;
																								tail = NULL;
																}

																// delete the list
																~DoubleList( ) {
																								// remove first one until there is no first one
																								Node* temp;
																								while(head != NULL) {
																																temp = head->next;
																																delete head;
																																head = temp;

																								}
																}


																// add an element to the end
																void addAtEnd(int number) {
																								// make the node
																								Node* node = new Node;
																								node->data = number;
																								node->next = NULL;

																								// if tail is NULL, it's the ONLY one
																								if(tail == NULL) {
																																head = node;
																																tail = node;
																																node->prev = NULL;
																								} else {
																																node->prev = tail;
																																tail->next = node;
																																tail = node;
																								}
																}

																// remove an element
																void remove(int number) {
																								// find the node with our number
																								Node* current = head;

																								while(current != NULL) {
																																if(current->data == number) {
																																								// FOUND IT

																																								// if the previous is NULL, we are removing head!
																																								if(current->prev == NULL) {
																																																head = current->next;
																																																if(head != NULL) {
																																																								head->prev = NULL;
																																																} else {
																																																								tail = NULL;
																																																}
																																								} else {
																																																// point previous's next pointer at the one after current
																																																current->prev->next = current->next;

																																																// also point next's previous pointer to current's prev
																																																if(current->next != NULL) {
																																																								current->next->prev = current->prev;
																																																} else {
																																																								tail = current->prev;
																																																}
																																								}

																																																delete current;
																																								// return
																																								return;
																																}

																																current = current->next;
																								}

																								// if we got here we didn't find it!
																								cout << number << " was not found in the list!" << endl;
																}

																// print the list Forward
																void printForward() const {
																								// the node we are currently on
																								Node* current = head;

																								// while we're not at the end
																								while(current != NULL) {
																																// print the data portion
																																cout << current->data << endl;

																																// move onto the next one
																																current = current->next;
																								}

																								cout << endl << endl;
																}

								private:
																struct Node {
																								int data;
																								Node* next;
																								Node* prev;
																};

																Node* head;
																Node* tail;
};

int main() {
								int num;
								DoubleList list;

								// adds 1 through 25 to list
								for (int i = 1; i <= 25; i++) {
																list.addAtEnd(i);
								}

								// remove the odd numbers
								for (int i = 1; i <= 25; i += 2) {
																list.remove(i);
								}

								// print out the list
								cout << "List:" << endl;
								list.printForward();

								return 0;
}


