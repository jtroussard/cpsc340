#include <cassert>
#include <iostream>
#include <fstream>
using namespace std;

// a point
struct Point {
    Point(int x = 0, int y = 0) {
        this->x = x;
        this->y = y;
    }

    int x, y;
};


template <typename type>
class ListStack {
    public:
        // start empty
        ListStack() {
            top = NULL;
        }

        // remove it all
        ~ListStack() {
            while(!empty()) {
                pop();
            }
        }

        // push something new on top
        void push(type new_data) {
            // make a new node
            Node* new_node = new Node;
            new_node->data = new_data;

            // put this one on top
            new_node->underneath = top;
            top = new_node;
        }

        // pop something off
        type pop() {
			cout << "(" << top->data.x << "," << top->data.y << ")" << endl;
            // save the value
            type top_data = top->data;

            // save the pointer
            Node* to_delete = top;

            // move top to the underneath one
            top = top->underneath;

            // remove the old top
            delete to_delete;

            // return the value
            return top_data;
        }

        // return whether or not we're empty
        bool empty() const {
            return top == NULL;
        }

		Point getTop() const {
			return top->data;
		}


    private:
        struct Node {
            type data;
            Node* underneath;
        };

        Node* top;
};


int main(int argc, char* argv[]) {
    ListStack<Point> stack;

    // declare the maze
    char** maze;
    int size;

    // open the file
    ifstream input;
    if(argc < 2) {
        cout << "Error, must give a file" << endl;
        return 0;
    }
    input.open(argv[1]);
    if(!input.is_open()) {
        cout << "Error, file not found" << endl;
        return 0;
    }

    // read the size first
    input >> size;

    // allocate space
    maze = new char*[size];
    for(int i = 0; i < size; i++) {
        maze[i] = new char[size];
    }

    // read each character
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            input >> maze[i][j];
        }
    }

    // start at the top left
    Point current;
    current.x = 0;
    current.y = 0;
    int steps = 0;

    // while we aren't at the end
    while((current.x != (size - 1)) || (current.y != (size - 1))) {
        steps++;

        // mark this spot as seen
        maze[current.y][current.x] = 'X';

        // if we haven't gone left, and not out of bounds, add left
        if(((current.x - 1) >= 0) && (maze[current.y][current.x - 1] == '-'))
            stack.push(Point(current.x - 1, current.y));

        // add right
        if(((current.x + 1) <= (size - 1)) && (maze[current.y][current.x + 1] == '-'))
            stack.push(Point(current.x + 1, current.y));

        // add bottom
        if(((current.y + 1) <= (size - 1)) && (maze[current.y + 1][current.x] == '-'))
            stack.push(Point(current.x, current.y + 1));

        // add top
        if(((current.y - 1) >= 0) && (maze[current.y - 1][current.x] == '-'))
			stack.push(Point(current.x, current.y - 1));

        // if the stack is empty, we have no path!
        if(stack.empty()) {
            cout << "No path!" << endl;
            return 0;
        }

        // get the next one from the stack
        current = stack.pop();
    }


    cout << "Maze completed after " << steps << " steps!" << endl;

    // delete the maze
    for(int i = 0; i < size; i++) {
        delete [] maze[i];
    }
    delete [] maze;

    return 0;
}

