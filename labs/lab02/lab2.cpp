#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>
using namespace std;

int main() {
	//create file variable
	ifstream infile("input.txt");

	//create line and line counter variables
	char line[100];
	int back;
	int cstringLength;

	//loop through file variable
	while(infile.getline(line, 101)) {
		//used as param for for loop and last index variable set up
		cstringLength = strlen(line);
		//used to keep track of rear index position of char to be compared from
		back = cstringLength-1;
		//used to identify which lines are palidromes
		int matches = 0;
		int total = 0;

		/*
		 * start loop through line. while index is alpha check corresponding
		 * index for alpha, if corresponding index is no alpha, increment the
		 * back variable and rechk until comparison can be made. each comparison
		 * triggers total var to increment, additionally each match will trigger
		 * match var to increment. Individual iterations through the for loop
		 * are controlled by the difference in size between the forward index 
		 * and the back variable. Once index is larger than back sufficient
		 * comparisons have been made to determine success or not.
		 */
		for (int i = 0; i < cstringLength; i++){
			//cout << "back: " << back << endl;
			//cout << "indx: " << i << endl;
			if (isalpha(line[i])){
				//cout << "line[i]: " << line[i] << endl;
				while (!isalpha(line[back])){
					back--;
					//cout << "line[back]: " << line[back] << endl;
				}
				if (tolower(line[i])==tolower(line[back])){
					//cout << line[i] << " matches " << line[back] << endl;
					matches++;
					total++;
				}else{
					//cout << "mismatch" << endl;
					total++;
				}
				back--;
			}
		}
		if (total != 0){
			if (matches != total){
				cout << "No" << endl;
			}else{
				cout << "Yes" << endl;
			}
		}

	}
	return 0;
}
