/* 
 * University of Mary Washington
 * Jacques J. Troussard
 * Prof. I.Finlayson
 * CPSC340 Lab 8
 * 02/27/17
 */

#include <iostream>
#include <cassert>

using namespace std;

class ListStack {
    public:
        // start empty
        ListStack() {
            top = NULL;
        }

        // delete the stack
        ~ListStack() {
            while (top != NULL) {
                pop();
            }
        }

        // push something new on top
        void push(int new_data) {
            Node* newTop = new Node;
            newTop->data = new_data;
            newTop->underneath = top;
            top = newTop;
        }

        // pop something off
        int pop() {
            int temp = top->data;
            Node* temp2 = top;
            top = top->underneath;
            delete temp2;
            return temp;
        }

        // return whether or not we're empty
        bool empty() const {
            return top == NULL;
        }

    private:
        struct Node {
            int data;
            Node* underneath;
        };
        Node* top;
};

int main() {
    // make the stack, declare bit and decimal number variables
    ListStack stack;
    int bit;
    int number;

	// get base ten number from input
	cout << "Enter a decimal number: ";
	cin >> number;

    // read in the numbers by digit, convert to binary and add them to the stack
    do {
        bit = number % 2;
        number /= 2;
        stack.push(bit);
    } while (number > 0);

    // pop bits from stack and print to console
    while(!stack.empty()) {
		cout << stack.pop();
    }
	cout << endl;

    return 0;
}
