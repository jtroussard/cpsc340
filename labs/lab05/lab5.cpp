// segfault.cpp
// generates a few segfaults

#include <iostream>
#include <cstring>
using namespace std;

int main() {
    // write to location 0
    //int* x = NULL;
    //*x = 12;

    // overrun an C-string really far
    char name1[] = "John Jacobs";
    for(int i = 0; i < 1000000000; i++) {
        cout << name1[i];
    }

    return 0;
}

