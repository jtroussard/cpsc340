// list.cpp

#include <iostream>
using namespace std;

template <typename type>
class List {
    struct Node;
    public:
    // make an empty list
    List() {
        head = NULL;
    }

    // delete the list
    ~List() {
        Node* current = head;
        Node* to_remove = NULL;

        while(current != NULL) {
            // save pointer to current
            to_remove = current;

            // move to the next one
            current = current->next;

            // delete last one
            delete to_remove;
        }
    }

    // add an element
    void add(type t) {
        // make a new node
        Node* node = new Node;

        // set the value
        node->data = t;

        // make this node point to the original head
        node->next = head;

        // point head to the new node
        head = node;
    } 

    // print the list forwards
    void printForwardHelper(Node* node) const {
        // fill in!
        if (node!=NULL) {
            cout << node->data << endl;
            return printForwardHelper(node->next);
        }


    }


    // print the list backwards`
    void printBackwardHelper(Node* node) const {
        // fill in!
        if (node!=NULL) {
            printBackwardHelper(node->next);
            cout << node->data << endl;
        }






    }

    // function to print starting at the head
    void printForward() const {
        // print starting at the head
        printForwardHelper(head);
    }

    // function to print starting at the head
    void printBackward() const {
        // print starting at the head
        printBackwardHelper(head);
    }

    private:
    struct Node {
        type data;
        Node* next;
    };

    Node* head;
};


int main() {
    // make a list
    List<int> list;

    // add some nodes
    for(int i = 10;  i >= 1; i--) {
        list.add(i);
    }

    // print the list forward
    cout << "Forward:" << endl;
    list.printForward();

    // print it backward
    cout << endl << "Backward:" << endl;
    list.printBackward();
    cout << endl;

    return 0;
}
