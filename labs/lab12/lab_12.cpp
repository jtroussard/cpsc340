/*  Jacques Troussard
	University of Mary Washington
	Prof. I.Finlayson
	CPSC340 Lab 12
	Power Function with recursion
*/

#include <iostream>
using namespace std;

// create function to check user input
int isPositive() {
	// prompt user for input
	int number;
	cout << "Enter a positive number: ";
	cin >> number;

	// if less than or equal to 0 scold user and set input var to recursive function
	if (number <= 0) {
		cout << "I said a positive number" <<endl;
		number = isPositive();
	}
	// once input variable skips if condition, return the number
	return (number);
}

// create function to calculate base and powers
int expntl(int base, int power) {
	// this is the work that needs to be repeated
	if (power > 1){
		// for even powers
		if (power % 2 == 0 && power != 0){
			return expntl(base, power/2) * expntl(base, power/2);
		// for odd powers
		} else {			
			return expntl(base, power/2) * expntl(base, power/2) * 2;
		}
	}
	/* the base case is the opposite of the if conditional, once the power is reduced
	to 1, it must return the base because, X^1 is always X. */
	return base;
}


int main(int argc, char const *argv[])
{
	int base = isPositive();
	int power = isPositive();
	int result = 0;

	result = expntl(base, power);
	cout << base << "^" << power << " = " << result << endl;

	
	return 0;
}
