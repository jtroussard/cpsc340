#include <iostream>
using namespace std;

template <typename type>
class List {
    public:
        // construct an empty linked list
        List() {
            head = NULL;
            count = 0;
        }

        // the destructor removes all of the nodes that have been allocated
        ~List() {
            clear();
        }

        // copy a new list from another one
        List(const List& other) {
            //start with an empty list
            head = NULL;

            // copy the other ones in
            Node* current = other.head;
            while (current != NULL) {
                addEnd(current->data);
                current = current->next;
            }
        }

        // assign an existing list the nodes of another one
        List operator=(const List& other) {
            // prevent self-assignment
            if (this == &other) {
                return *this;
            }

            // clear the existing nodes
            clear();

            // copy the other ones in
            Node* current = other.head;
            while (current != NULL) {
                addEnd(current->data);
                current = current->next;
            }

            return *this;
        }

        // function which clears all of the nodes from the list
        void clear() {
            // delete the head node until there is no head left
            while (head != NULL) {
                Node* temp = head;
                head = head->next;
                delete temp;
                count--;
            }
        }

        // add a new value to the end
        void addEnd(type value) {
            if (head == NULL) {
                head = new Node;
                head->data = value;
                head->next = NULL;
            } else {
                Node* prev = head;
                Node* curr = head->next;
                while (curr != NULL) {
                    prev = curr;
                    curr = curr->next;
                }
                prev->next = new Node;
                prev->next->data = value;
                prev->next->next = NULL;
            }
            count++;
        }

        // add a new value at the start
        void addStart(type value) {
            Node* newNode = new Node;
            newNode->next = head;
            head = newNode;
            newNode->data = value;
            count++;
        }

        void remove(type value) {
            // if the list is empty, do nothing
            if (head == NULL) {
                return;

            // if the item to remove is the first one
            } else if (head->data == value) {
                Node* temp = head;
                head = head->next;
                delete temp;

            // else we need to find the item
            } else {
                // keep track of the current and the one before it
                Node* previous = head;
                Node* current = head->next;

                while (current != NULL) {
                    // if this item is the one we're looking for
                    if (current->data == value) {
                        // fix the link before it to point to the next one
                        previous->next = current->next;
                        // remove it from memory
                        delete current;
                        break;
                    }
                    
                    // move on to the next element
                    previous = current;
                    current = current->next;
                }
            }
            count--;
        }

        // print all of the nodes in the list
        void print() {
            Node* current = head;
            while (current != NULL) {
                cout << current->data << endl;
                current = current->next;
            }
        }

        int length() const {
            return count;
        }

    private:
        struct Node {
            type data;
            Node* next;
        };

        Node* head;
        int count;
};


int main() {
    List <char> list;

    // put in the numbers 65 through 90
    for (int i = 65; i <= 90; i++) {
        list.addEnd(i);
    }

    cout << "List has " << list.length() << " items:" << endl;
    list.print();

    return 0;
}
