# Computer Science 340: Data Structures and Algorithms #

Course Description: Continued study of data modeling and incorporation of abstract data types including linked lists, stacks, queues, heaps, trees, and graphs. Study of advanced sorting and searching techniques. Provides experience in the use of algorithm analysis. Continued study of program design, coding, debugging, testing, and documentation in an object-oriented higher level language..

### What is this repository for? ###

* Storing course work for CPSC340
 - Labs & Assignments
 - Some snippets and loose tests

### How do I get set up? ###

* Labs are stand alone files .
* Assignment 3 and onward come with make-files.